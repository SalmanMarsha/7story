from django.test import TestCase,Client,LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from .views import index, confirmation, change
from .models import Status
from .forms import FormStatus
from .apps import Story7Config

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.
class UrlRouting(TestCase):
    def test_url1_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_url1_using_landing_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_ur1l_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_url2_exists(self):
        response = Client().post('/confirmation/', {'nama':'dum', 'pesan':'posty'})
        self.assertEqual(response.status_code,200)

    def test_url2_using_confirmation_func(self):
        found = resolve('/confirmation/')
        self.assertEqual(found.func, confirmation)
    
    def test_url3_exists(self):
        abc = Status.objects.create(nama="atta", pesan="ashiap")
        index = "/" +str(abc.id) + '/'
        response = Client().post(index)
        self.assertEqual(response.status_code,302)
    
    def test_url3_using_change_func(self):
        abc = Status.objects.create(nama="attb", pesan="ashiat")
        index = "/" +str(abc.id) + '/'
        found = resolve(index)
        self.assertEqual(found.func,change)

class Model(TestCase):
    def test_model_can_create(self):
        model = Status(nama="salman", pesan="baik kabar saya")
        model.save()
        self.assertEqual(Status.objects.all().count(), 1)

class Form(TestCase):
    def test_form(self):
        form = FormStatus({'nama': 'Salman', 'pesan': 'test pesan'})
        self.assertTrue(form.is_valid())
        form = FormStatus(data={})
        self.assertFalse(form.is_valid())
    
class Apps(TestCase):
    def test_app(self):
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7')

class Story7FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.browser.quit()
        super(Story7FunctionalTest, self).tearDown()
    
    def test_confirming_status(self):
        self.browser.get(self.live_server_url)
        nama = self.browser.find_element_by_name('nama')
        nama.send_keys('gita')

        pesan = self.browser.find_element_by_name('pesan')
        pesan.send_keys('ini pesasn')

        submit = self.browser.find_element_by_name('submit')
        submit.click()
        
        #bakal ke confirm_page
        confirm = self.browser.find_element_by_name("iya")
        confirm.click()

        self.assertIn('gita',self.browser.page_source)
        self.assertIn('ini pesasn', self.browser.page_source)
    
    def test_canceling_status(self):
        self.browser.get(self.live_server_url)
        nama = self.browser.find_element_by_name('nama')
        nama.send_keys('gitb')

        pesan = self.browser.find_element_by_name('pesan')
        pesan.send_keys('ini pesasnn')

        submit = self.browser.find_element_by_name('submit')
        submit.click()
        
        #bakal ke confirm_page
        confirm = self.browser.find_element_by_name("ngga")
        confirm.click()

        self.assertNotIn('gitb',self.browser.page_source)
        self.assertNotIn('ini pesasnn', self.browser.page_source)
    
    def test_widow(self):
        self.browser.get(self.live_server_url)
        nama = self.browser.find_element_by_name('nama')
        nama.send_keys('sayur')

        pesan = self.browser.find_element_by_name('pesan')
        pesan.send_keys('pesan mang')

        submit = self.browser.find_element_by_name('submit')
        submit.click()
        
        #bakal ke confirm_page
        confirm = self.browser.find_element_by_name("iya")
        confirm.click()

        #menyimpan background yg pertama
        holder = self.browser.find_element_by_class_name('boxx').value_of_css_property('background-color')

        #disubmit untuk mengganti background
        color = self.browser.find_element_by_name('submits')
        color.click()

        self.assertNotEqual(holder, self.browser.find_element_by_class_name('boxx').value_of_css_property('background-color'))