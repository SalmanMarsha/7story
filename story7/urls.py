from django.urls import path
from . import views

app_name = 'story7'

urlpatterns = [
    path('', views.index, name='index'),
    path('confirmation/', views.confirmation, name="confirmation"),
    path('<int:id>/',views.change, name="change")
]