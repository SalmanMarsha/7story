from django.forms import ModelForm
from .models import Status

class FormStatus(ModelForm):
	class Meta:
		model = Status
		fields = ["nama","pesan"]
		
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		for field in iter(self.fields):
			self.fields[field].widget.attrs.update({
				'class': 'form-control'
		}
		)
