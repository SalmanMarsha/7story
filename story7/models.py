from django.db import models

# Create your models here.
class Status(models.Model):
    nama = models.CharField(max_length=100)
    pesan = models.TextField()
    color = models.CharField(max_length=100, default='#000000')