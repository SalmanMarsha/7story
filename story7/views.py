from django.shortcuts import render, redirect
from .models import Status
from .forms import FormStatus
import random

# Create your views here.
def index(request):
    form = FormStatus()
    if request.method == 'POST':
        nama = request.POST['nama']
        pesan = request.POST['pesan']
        Status.objects.create(nama=nama,pesan=pesan)

    bajan = Status.objects.all().order_by('id')
    return render(request,'landing.html',{'form': form,'list':bajan})

def confirmation(request):
    if request.method == "POST":
        form = FormStatus(request.POST)
        return render(request, 'confirm_page.html', {'nama': form.data['nama'], 'pesan' : form.data['pesan']})

def change(request, id):
    if request.method == "POST":
        form = Status.objects.get(pk = id)
        a = str(hex(random.randint(0,255)))[2:]
        b = str(hex(random.randint(0,255)))[2:]
        c = str(hex(random.randint(0,255)))[2:]
        color = a+b+c
        fix = "#" + color
        form.color = fix
        form.save()
        return redirect('/')